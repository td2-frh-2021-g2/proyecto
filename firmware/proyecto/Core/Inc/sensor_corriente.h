/*
 * sensor_corriente.h
 *
 *  Created on: Nov 22, 2021
 *      Author: ftambara
 */

#ifndef INC_SENSOR_CORRIENTE_H_
#define INC_SENSOR_CORRIENTE_H_

#define TIEMPO_MAX_N_MUESTRAS_MS	20	//tiempo para error de timeout, mucho mas grande de lo esperado

void sensor_corriente_init(void);
void task_medir_sensor_corriente (void *parametros);
void task_procesar_muestra (void *parametros);

#endif /* INC_SENSOR_CORRIENTE_H_ */
