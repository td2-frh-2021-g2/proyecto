/*
 * usergpio.h
 *
 *  Created on: 5 nov. 2021
 *      Author: nahueldb
 */

#ifndef INC_USER_GPIO_H_
#define INC_USER_GPIO_H_

void GPIO_habilitar_alimentacion_equipo(void);
void GPIO_interrumpir_alimentacion_equipo(void);

/** @brief Prende el LED de la BP*/
void prender_led_bp();
/** @brief Apaga el LED de la BP*/
void apagar_led_bp(void);

/** @brief Togglea el LED de la BP*/
void toggle_led(void);

void set_esp_en(void);
void reset_esp_en(void);

#endif /* INC_USER_GPIO_H_ */
