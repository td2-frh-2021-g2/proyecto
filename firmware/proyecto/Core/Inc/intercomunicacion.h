/*
 * comunicacion.h
 *
 *  Created on: Nov 28, 2021
 *      Author: ftambara
 */

#ifndef INC_INTERCOMUNICACION_H_
#define INC_INTERCOMUNICACION_H_

#include "cmsis_os.h"
#include "queue.h"
#include "semphr.h"

typedef enum
{
	MUESTRA,
	ALERTA_MEDICIONES_NULAS,
	ALERTA_MEDICIONES_INESTABLES
} esp_tipo_de_dato_t;

typedef struct
{
	esp_tipo_de_dato_t tipo;
	uint32_t dato;
} esp_dato_t;

void intercomunicacion_init(void);

QueueHandle_t queue_esp_enviar;

#endif /* INC_INTERCOMUNICACION_H_ */
