/*
 * fsmEsp.h
 *
 *  Created on: Jun 4, 2021
 *      Author: nahueldb
 */

#ifndef INC_ESP8266_H_
#define INC_ESP8266_H_
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

/** @brief Inicializa objetos de FreeRTOS para la ESP*/
void esp8266_init(void);

/** @brief Task encargada del funcionamiento de la ESP tanto conexion como envio de datos*/
void task_esp8266(void *arg);

/** @brief Task encargada de la gestion de la pagina web*/
void task_server(void *arg);



extern bool evEspListo;

#endif /* INC_ESP8266_H_ */
