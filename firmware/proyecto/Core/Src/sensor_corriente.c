/**
  ******************************************************************************
  * @file           : sensor_corriente.c
  * @brief          : Control del sensado de corriente.
  *
  ******************************************************************************
  */

/*==================[inclusions]=============================================*/
#include "sensor_corriente.h"

#include "main.h"
#include "cmsis_os.h"
#include "task.h"
#include "semphr.h"
#include "math.h"

#include "intercomunicacion.h"
#include "user_gpio.h"

/*==================[macros and definitions]=================================*/
#define ADC_MUESTRAS_N 100

#define VALOR_MAX_MUESTRA		4095	// pasos
#define VOLTAJE_MAX_SENSOR		3.3		// V
#define VOLTAJE_CERO_CORRIENTE	2.515		// V
#define SENSIBILIDAD_V_AMP		0.1		// V/A
#define VOLTAJE_AC				220		// V (rms)
#define RELACION_DIVISOR_RES	10.15/8.1

#define PROCESAMIENTO_N	10
#define UMBRAL_SIN_SUMINISTRO	10000	// miliwatts

/*==================[internal data declaration]==============================*/
/**
 * 	@brief	Da aviso de vector de muestras listo para ser procesado
 */
static SemaphoreHandle_t semphr_adc_done;

/**
 * 	@brief	Transferencia de muestra de **task_medir_sensor_corriente** a **task_procesar_muestra**
 */
static QueueHandle_t queue_muestra;

/*==================[internal functions declaration]=========================*/
static uint32_t pasos_a_watts(uint16_t muestra);

/*==================[internal data definition]===============================*/


/*==================[external data definition]===============================*/
extern ADC_HandleTypeDef hadc1;

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	xSemaphoreGiveFromISR(semphr_adc_done, NULL);
}

/** @brief	Convierte un valor en pasos de 0 a 4095 a potencia en miliwatts según funcionamiento del sensor ACS712ELCTR-20A-T
  *
  * @param  muestra en pasos
  * @return	muestra en watts
  * @note	tensión de línea estimada en 220 Vrms
  */
static uint32_t pasos_a_watts(uint16_t muestra)
{
	float muestra_watts = VOLTAJE_AC * ((float)muestra/VALOR_MAX_MUESTRA * VOLTAJE_MAX_SENSOR *RELACION_DIVISOR_RES)/SENSIBILIDAD_V_AMP;
	if (muestra_watts < 0)
		muestra_watts *= -1;

	return (uint32_t)(muestra_watts*1000);
}

/** @brief	Inicia objetos de FreeRTOS relativos al módulo sensor_corriente.c
  */
void sensor_corriente_init(void)
{
	queue_muestra = xQueueCreate(3, sizeof(uint16_t));
	semphr_adc_done = xSemaphoreCreateBinary();
	if (semphr_adc_done == NULL)
	{
		//Error de memoria
	}

	xTaskCreate(task_medir_sensor_corriente,
				"Sensar corriente",
				configMINIMAL_STACK_SIZE,
				NULL,
				osPriorityNormal,
				NULL);

	xTaskCreate(task_procesar_muestra,
				"Procesar muestras",
				configMINIMAL_STACK_SIZE,
				NULL,
				osPriorityNormal1,
				NULL);

}

/** @brief	Promedia el vector de muestras que recolectó el ADC y las envía a ser procesadas
 *
 *	@see	task_procesar_muestra()
  */
void task_medir_sensor_corriente (void *parametros)
{
	uint8_t i;
	BaseType_t estado_semphr;
	uint16_t adc_muestras[ADC_MUESTRAS_N];
	uint32_t muestras_sum;
	uint16_t muestras_media;

	HAL_ADCEx_Calibration_Start(&hadc1);
	HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adc_muestras, ADC_MUESTRAS_N);

	while (1)
	{
		estado_semphr = xSemaphoreTake(semphr_adc_done, pdMS_TO_TICKS(TIEMPO_MAX_N_MUESTRAS_MS));
		if (estado_semphr != pdPASS)
		{
			while (1);
			//Error de timeout, alertar.
		}
		/* Sumo muestras para obtener media */
		muestras_sum = 0;
		for (i=0; i<ADC_MUESTRAS_N; i++)
		{
			muestras_sum += powf(adc_muestras[i] - (VOLTAJE_CERO_CORRIENTE/(RELACION_DIVISOR_RES) / VOLTAJE_MAX_SENSOR) * VALOR_MAX_MUESTRA, 2);
		}
		muestras_media = (uint16_t)sqrt(muestras_sum/ADC_MUESTRAS_N);
		xQueueSend(queue_muestra, &muestras_media, portMAX_DELAY);
	}
}

/** @brief	Promedia una cantidad VECTOR_PROCESAMIENTO_N de muestras, la comunica, y envía las alertas correspondientes
 * 	@see	task_axl()
  */
void task_procesar_muestra (void *parametros)
{
	uint16_t muestra;
	static uint8_t i=0;
	static uint32_t suma_muestras = 0;
	static uint32_t	ultima_media = 0;
	esp_dato_t enviar;

	static uint8_t umbral_variacion = 10;	//Configurable, en porcentaje

	while (1)
	{
		xQueueReceive(queue_muestra, &muestra, portMAX_DELAY);

		suma_muestras += muestra;

		i++;
		if (i == PROCESAMIENTO_N )
		{
			if (suma_muestras / PROCESAMIENTO_N <= UMBRAL_SIN_SUMINISTRO
				|| -suma_muestras / PROCESAMIENTO_N <= UMBRAL_SIN_SUMINISTRO)
			{
				//GPIO_interrumpir_alimentacion_equipo();
				// Alerta, falta de suministro;
			}
			if ( suma_muestras / PROCESAMIENTO_N <= ultima_media * (100 - umbral_variacion)/100
				|| suma_muestras / PROCESAMIENTO_N >= ultima_media * (100 + umbral_variacion)/100 )
			{
				//Alerta variacion excesiva
			}
			enviar.tipo = MUESTRA;
			ultima_media = (uint32_t)pasos_a_watts(suma_muestras / PROCESAMIENTO_N);
			enviar.dato = ultima_media;
			xQueueSend(queue_esp_enviar, &enviar, pdMS_TO_TICKS(TIEMPO_MAX_N_MUESTRAS_MS));

			suma_muestras = 0;
			i = 0;
		}
	}
}

/*==================[end of file]============================================*/
