/*
 * fsmEsp.c
 *
 *  Created on: Jun 4, 2021
 *      Author: nahueldb
 */
/* Copyright 2021, TD2-FRH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*==================[inclusions]=============================================*/

#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include "esp8266.h"

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "cmsis_os.h"
#include "task.h"
#include "semphr.h"

#include "main.h"
#include "user_gpio.h"
#include "intercomunicacion.h"

/*==================[macros and definitions]=================================*/

#define RX_BUF_LEN 512
#define AUX_BUF_LEN 512
#define PAGE_TX 1024
#define READY_RX_ESP "ready\r\n"
#define AT_TX_ESP "AT\r\n"
#define AT_RST_TX_ESP "AT+RST\r\n"
#define OK_RX_ESP "OK\r\n"
#define WIFI_CONNECTED_RX_ESP "WIFI CONNECTED"
#define WIFI_GOTIP_RX_ESP "WIFI GOT IP"
#define CONNECT_RX_ESP "CONNECT"
#define AT_CWMODE_TX_ESP "AT+CWMODE=1\r\n"
#define AT_CWJAP_TX_ESP "AT+CWJAP=\"Speedy-SHAN_EXT\",\"Carmine-1920\"\r\n"
#define AT_CIPSTART_TX_ESP "AT+CIPSTART=\"UDP\",\"192.168.43.77\",8000,8001\r\n"
#define AT_CIPSEND_TX_ESP "AT+CIPSEND="
#define AT_SENDOK_RX_ESP "SEND OK"
#define IPD_RX_ESP "+IPD,"
#define AT_CIFSR_TX_ESP "AT+CIFSR\r\n"
#define AT_CIFSR_RX_ESP "CIFSR:STAIP,\""
#define AT_CIPMUX_TX_ESP "AT+CIPMUX=1\r\n"
#define AT_CIPSERVER_TX_ESP "AT+CIPSERVER=1,80\r\n"
#define AT_CWQAP_TX_ESP "AT+CWQAP\r\n"
#define CONNECT ",CONNECT"
#define CONNECT0 "0,CONNECT"
#define CONNECT1 "1,CONNECT"
#define CONNECT2 "2,CONNECT"
#define CONNECT3 "3,CONNECT"
#define CONNECT4 "4,CONNECT"
#define CONNECT5 "5,CONNECT"
#define AT_CIPCLOSE_TX_ESP "AT+CIPCLOSE="
#define CRLF "\r\n"
#define CMD_CONEXION "/on"
#define CMD_DESCONEXION "/off"
#define CMD_CONSUMO "/sensar"
#define CMD_CONSUMO_ALT "/consumo"

/*==================[internal data declaration]==============================*/
/** @brief Enumeracion de todos los comandos que se pueden recibir de la pagina web*/
typedef enum{
	CONEXION,
	DESCONEXION,
	CONSUMO,
	CONSUMO_ALT,
	NONE
} comandos_t;
/** @brief Enumeracion de todos los estados de la task_esp8266*/
typedef enum {
	ESP_CONECTADA,
	ESPERANDO_SEMAFORO,
	ESPERANDO_READY,
    ESPERANDO_OK_CWQAP,
	ESPERANDO_OK_CWMODE,
	ESPERANDO_OK_CWJAP,
	ESPERANDO_OK_CIFSR,
	ESPERANDO_OK_CIPMUX,
	ESPERANDO_OK_CIPSERVER,
	ESPERANDO_MENSAJE,
	ENVIAR_MENSAJE,
	ENVIAR_CIPSEND,
	ESPERANDO_OK_CIPSEND,
	ESPERANDO_RECV
} conexion_esp_t;
/*==================[internal functions declaration]=========================*/
/** @brief Envio de datos a la pagina web
  *
  * @param  pùntero al dato a enviar
  * @param  longitud del dato
  */
void esp8266_tx(char * data, size_t len);

/**
  * @brief  Obtiene el comando mirando el buffer de entrada
  *
  * @param  Ultimo comando recibido
  * @retval Comando recibido del tipo COMANDOS_T
  */
comandos_t esp8266_rx_comando(comandos_t last_cmd_rx);
/*==================[internal data definition]===============================*/

//static COMANDOS_T comando_rx_enum;
uint8_t  rx_buf[RX_BUF_LEN], pag_tx[PAGE_TX], aux_buf_tx[AUX_BUF_LEN];

char *BASIC_INCLUSION = "<!DOCTYPE html> <html>\n<head><meta name=\"viewport\"\
		content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n\
		<title>Crypto Fenix</title>\n<style>html { font-family: Helvetica; \
		display: inline-block; margin: 0px auto; text-align: center;}\n\
		body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\
		h3 {color: #444444;margin-bottom: 50px;}\n.button {display: block;\
		width: 80px;background-color: #1abc9c;border: none;color: white;\
		padding: 13px 30px;text-decoration: none;font-size: 25px;\
		margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n\
		.button-on {background-color: #1abc9c;}\n.button-on:active \
		{background-color: #16a085;}\n.button-off {background-color: #34495e;}\n\
		.button-off:active {background-color: #2c3e50;}\np {font-size: 14px;color: #888;margin-bottom: 10px;}\n\
		</style>\n</head>\n<body>\n<h1>ESP8266 LED CONTROL</h1>\n";

char *CONEXION_PAGE = "<!DOCTYPE html> <html>\n<head><meta name=\"viewport\"\
		content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n\
		<title>TD2G2</title>\n<style>html { font-family: Helvetica; \
		display: inline-block; margin: 0px auto; text-align: center;}\n\
		body{margin-top: 50px;} h1 {}\
		h3 {}\n.button {display: block;\
		width: 110px;background-color: #1abc9c;border: none;color: white;\
		padding: 13px 30px;text-decoration: none;font-size: 25px;\
		margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n\
		.button-on {background-color: #1abc9c;}\n.button-on:active \
		{background-color: #16a085;}\n.button-off {background-color: #34495e;}\n\
		.button-off:active {background-color: #2c3e50;}\np {font-size: 14px;color: #888;margin-bottom: 10px;}\n\
		</style>\n</head>\n<body>\n<h1>CRYPTO FENIX</h1>\n<h4>Status: Conectado</h4>\
		<a class=\"button button-off\" href=\"/off\">OFF</a><a class=\"button button-on\" href=\"/sensar\">SENSAR</a>\n<h4>Consumo: ";

char *CONEXION_ALT_PAGE = "<!DOCTYPE html> <html>\n<head><meta name=\"viewport\"\
		content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n\
		<title>TD2G2</title>\n<style>html { font-family: Helvetica; \
		display: inline-block; margin: 0px auto; text-align: center;}\n\
		body{margin-top: 50px;} h1 {}\
		h3 {}\n.button {display: block;\
		width: 110px;background-color: #1abc9c;border: none;color: white;\
		padding: 13px 30px;text-decoration: none;font-size: 25px;\
		margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n\
		.button-on {background-color: #1abc9c;}\n.button-on:active \
		{background-color: #16a085;}\n.button-off {background-color: #34495e;}\n\
		.button-off:active {background-color: #2c3e50;}\np {font-size: 14px;color: #888;margin-bottom: 10px;}\n\
		</style>\n</head>\n<body>\n<h1>CRYPTO FENIX</h1>\n<h4>Status: Conectado</h4>\
		<a class=\"button button-off\" href=\"/off\">OFF</a><a class=\"button button-on\" href=\"/consumo\">SENSAR</a>\n<h4>Consumo: ";

char *DESCONEXION_PAGE = "<!DOCTYPE html> <html>\n<head><meta name=\"viewport\"\
		content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n\
		<title>TD2G2</title>\n<style>html { font-family: Helvetica; \
		display: inline-block; margin: 0px auto; text-align: center;}\n\
		body{margin-top: 50px;} h1 {}\
		h3 {}\n.button {display: block;\
		width: 110px;background-color: #1abc9c;border: none;color: white;\
		padding: 13px 30px;text-decoration: none;font-size: 25px;\
		margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n\
		.button-on {background-color: #1abc9c;}\n.button-on:active \
		{background-color: #16a085;}\n.button-off {background-color: #34495e;}\n\
		.button-off:active {background-color: #2c3e50;}\np {font-size: 14px;color: #888;margin-bottom: 10px;}\n\
		</style>\n</head>\n<body>\n<h1>CRYPTO FENIX</h1>\n<h4>Status: Desconectado</h4>\
		<a class=\"button button-on\" href=\"/on\">ON</a></body></html>                                                                                          ";

char *FINAL_CONEXION = " mW</h4></body></html>";

/*==================[external data definition]===============================*/
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
static SemaphoreHandle_t semphr_esp8266_cpltcall;
static SemaphoreHandle_t semphr_esp8266_msjs;
static SemaphoreHandle_t semphr_esp8266_conexion;
/*==================[internal functions definition]==========================*/

void esp8266_tx(char * data, size_t len)
{
	memcpy(pag_tx, data, len > PAGE_TX ? PAGE_TX : len);
	xSemaphoreGive(semphr_esp8266_msjs);

}

comandos_t esp8266_rx_comando(comandos_t last_cmd_rx)
{
	if (memmem(rx_buf, RX_BUF_LEN, CMD_CONEXION, strlen(CMD_CONEXION)) && (last_cmd_rx != CONEXION))
	{
		return CONEXION;
	}
	else if (memmem(rx_buf, RX_BUF_LEN, CMD_DESCONEXION, strlen(CMD_DESCONEXION)) && (last_cmd_rx != DESCONEXION))
	{
		return DESCONEXION;
	}
	else if (memmem(rx_buf, RX_BUF_LEN, CMD_CONSUMO, strlen(CMD_CONSUMO)) && (last_cmd_rx != CONSUMO))
	{
		return CONSUMO;
	}
	else if (memmem(rx_buf, RX_BUF_LEN, CMD_CONSUMO_ALT, strlen(CMD_CONSUMO_ALT)) && (last_cmd_rx != CONSUMO_ALT))
	{
		return CONSUMO_ALT;
	}
	else
	{
		return NONE;
	}

}

/*==================[external functions definition]==========================*/

void esp8266_init(void)
{
	BaseType_t estado_creacion;

	/* Semaforos */
	semphr_esp8266_cpltcall = xSemaphoreCreateBinary();
	semphr_esp8266_msjs = xSemaphoreCreateBinary();
	semphr_esp8266_conexion = xSemaphoreCreateBinary();

	if (semphr_esp8266_cpltcall == NULL)
		while (1);
		//Error de memoria

	if (semphr_esp8266_msjs == NULL)
		while (1); //Error de memoria

	if (semphr_esp8266_conexion == NULL)
		while (1); //Error de memoria

	/* Tasks */
	estado_creacion = xTaskCreate(task_esp8266,
			                      "Conexion",
								  configMINIMAL_STACK_SIZE,
								  NULL,
								  osPriorityNormal,
								  NULL);
	if (estado_creacion == pdFAIL)
		while (1); //Error de memoria

	estado_creacion = xTaskCreate(task_server,
								  "Server",
								  configMINIMAL_STACK_SIZE,
								  NULL,
								  osPriorityNormal1,
								  NULL);
	if (estado_creacion == pdFAIL)
		while (1); //Error de memoria
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	xSemaphoreGiveFromISR(semphr_esp8266_cpltcall,NULL);
}

void task_esp8266(void *arg)
{
	//uint8_t aux_buf_tx[AUX_BUF_LEN];
	conexion_esp_t pasos_conexion, paso_siguiente;
	char *ptr_connect = NULL;
	uint8_t port_connect;

	pasos_conexion = ESPERANDO_READY;

	/* Reset ESP */
	reset_esp_en();
	vTaskDelay(pdMS_TO_TICKS(1000));
	set_esp_en();

	vTaskDelay(100);

	while (1)
	{
		//vTaskDelay(100);
		HAL_UART_Receive_IT(&huart1, rx_buf, RX_BUF_LEN);
		switch(pasos_conexion)
		{
		case ESPERANDO_READY:
			if (memmem(rx_buf,RX_BUF_LEN, READY_RX_ESP, strlen(READY_RX_ESP))/*&&
				memmem(rxBuf,RX_BUF_LEN, WIFI_CONNECTED_RX_ESP, strlen(WIFI_CONNECTED_RX_ESP))&&
				memmem(rxBuf,RX_BUF_LEN, WIFI_GOTIP_RX_ESP, strlen(WIFI_GOTIP_RX_ESP))*/)
			{
				bzero(rx_buf,RX_BUF_LEN);
				//HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CWQAP_TX_ESP, strlen(AT_CWQAP_TX_ESP));//"AT+CWQAP\r\n"
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CWMODE_TX_ESP, strlen(AT_CWMODE_TX_ESP));//"AT+CWMODE=1\r\n"
				pasos_conexion = ESPERANDO_SEMAFORO;
				//paso_siguiente = ESPERANDO_OK_CWQAP;
				paso_siguiente = ESPERANDO_OK_CWMODE;
				vTaskDelay(100);
			}
			break;
		/*case ESPERANDO_OK_CWQAP://Desconecto el WIFI por si quedo conectada anteriormente
			if (memmem(rxBuf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP)))
			{
				bzero(rxBuf,RX_BUF_LEN);
				vTaskDelay(100);
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CWMODE_TX_ESP, strlen(AT_CWMODE_TX_ESP));//"AT+CWMODE=1\r\n"
				pasos_conexion = ESPERANDO_SEMAFORO;
				paso_siguiente = ESPERANDO_OK_CWMODE;
			}
			break;*/
		case ESPERANDO_OK_CWMODE:
			if (memmem(rx_buf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP)))
			{
				vTaskDelay(100);
				bzero(rx_buf,RX_BUF_LEN);
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CWJAP_TX_ESP, strlen(AT_CWJAP_TX_ESP));//"AT+CWJAP=\"SSID\",\"PASS\"\r\n"
				pasos_conexion = ESPERANDO_SEMAFORO;
				paso_siguiente = ESPERANDO_OK_CWJAP;
			}
			break;
		case ESPERANDO_OK_CWJAP:
			if (memmem(rx_buf,RX_BUF_LEN, WIFI_CONNECTED_RX_ESP, strlen(WIFI_CONNECTED_RX_ESP))&&
				memmem(rx_buf,RX_BUF_LEN, WIFI_GOTIP_RX_ESP, strlen(WIFI_GOTIP_RX_ESP))&&
				memmem(rx_buf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP)))
			{
				bzero(rx_buf,RX_BUF_LEN);
				vTaskDelay(100);
				//HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CIFSR_TX_ESP, strlen(AT_CIFSR_TX_ESP));//"AT+CIFSR\r\n"
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CIPMUX_TX_ESP, strlen(AT_CIPMUX_TX_ESP));//"AT+CIPMUX=1\r\n"
				pasos_conexion = ESPERANDO_SEMAFORO;
				//paso_siguiente = ESPERANDO_OK_CIFSR;
				paso_siguiente = ESPERANDO_OK_CIPMUX;
			}
			break;
		/*case ESPERANDO_OK_CIFSR:
			if (memmem(rxBuf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP)))
			{
				//HAL_UART_Transmit_IT(&huart2, (uint8_t *) rxBuf, strlen((char *) rxBuf));//Obtengo IP
				//xSemaphoreTake(semaforo_cpltcall, portMAX_DELAY);
				bzero(rxBuf,RX_BUF_LEN);
				vTaskDelay(100);
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CIPMUX_TX_ESP, strlen(AT_CIPMUX_TX_ESP));//"AT+CIPMUX=1\r\n"
				pasos_conexion = ESPERANDO_SEMAFORO;
				paso_siguiente = ESPERANDO_OK_CIPMUX;
			}
			break;*/
		case ESPERANDO_OK_CIPMUX:
			if (memmem(rx_buf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP)))
			{
				bzero(rx_buf,RX_BUF_LEN);
				//vTaskDelay(100);
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) AT_CIPSERVER_TX_ESP, strlen(AT_CIPSERVER_TX_ESP));//"AT+CIPSERVER=1,80\r\n"
				pasos_conexion = ESPERANDO_SEMAFORO;
				paso_siguiente = ESPERANDO_OK_CIPSERVER;
			}
			break;
		case ESPERANDO_OK_CIPSERVER:
			if (memmem(rx_buf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP)))
			{
				bzero(rx_buf,RX_BUF_LEN);
				vTaskDelay(100);
				vTaskPrioritySet(NULL, osPriorityNormal1);
				pasos_conexion = ESPERANDO_MENSAJE;
				xSemaphoreGive(semphr_esp8266_conexion);
			}
			break;
		case ESPERANDO_MENSAJE:
			xSemaphoreTake(semphr_esp8266_msjs, portMAX_DELAY);
			pasos_conexion = ENVIAR_CIPSEND;
			break;
		case ENVIAR_CIPSEND:
			ptr_connect = memmem(rx_buf,RX_BUF_LEN, CONNECT, strlen(CONNECT));
			if (ptr_connect != NULL)
			{
				port_connect = atoi(ptr_connect - 1);
				//vTaskDelay(pdMS_TO_TICKS(800));
				bzero(aux_buf_tx, AUX_BUF_LEN);
				snprintf((char*)aux_buf_tx, AUX_BUF_LEN, "%s%u%s%u%s", AT_CIPSEND_TX_ESP, port_connect, ",",strlen((const char*)pag_tx), CRLF);//"AT+CIPSEND=port,len/r/n"
				//bzero(rx_buf,RX_BUF_LEN);
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) aux_buf_tx, strlen((const char*)aux_buf_tx));
				pasos_conexion = ESPERANDO_SEMAFORO;
				paso_siguiente = ESPERANDO_OK_CIPSEND;
				//vTaskDelay(100);
			}
			break;
		case ESPERANDO_OK_CIPSEND:
			if (memmem(rx_buf,RX_BUF_LEN, OK_RX_ESP, strlen(OK_RX_ESP))/*&&
				memmem(rx_buf,RX_BUF_LEN, ">", 1)*/)
			{
				pasos_conexion = ENVIAR_MENSAJE;
				//vTaskDelay(pdMS_TO_TICKS(100));
			}
			vTaskDelay(50);
			break;
		case ENVIAR_MENSAJE:
			bzero(rx_buf,RX_BUF_LEN);
			HAL_UART_Transmit_IT(&huart1, (uint8_t *) pag_tx, strlen((const char*)pag_tx));//Envio pagina
			pasos_conexion = ESPERANDO_SEMAFORO;
			paso_siguiente = ESPERANDO_RECV;
			bzero(aux_buf_tx, AUX_BUF_LEN);
			snprintf((char*)aux_buf_tx, AUX_BUF_LEN, "%s %u %s", "Recv", strlen((const char*)pag_tx), "bytes");
			break;
		case ESPERANDO_RECV:
			if (memmem(rx_buf,RX_BUF_LEN, aux_buf_tx, strlen((const char*)aux_buf_tx))&&
				memmem(rx_buf,RX_BUF_LEN, AT_SENDOK_RX_ESP, strlen(AT_SENDOK_RX_ESP)))
			{
				bzero(rx_buf,RX_BUF_LEN);
				snprintf((char*)aux_buf_tx, AUX_BUF_LEN, "%s%u%s", AT_CIPCLOSE_TX_ESP, port_connect, CRLF);//"AT+CIPCLOSE=port/r/n"
				HAL_UART_Transmit_IT(&huart1, (uint8_t *) aux_buf_tx, strlen((const char*)aux_buf_tx));
				pasos_conexion = ESPERANDO_SEMAFORO;
				paso_siguiente = ESPERANDO_MENSAJE;
				xSemaphoreGive(semphr_esp8266_conexion);
			}
			break;
		case ESPERANDO_SEMAFORO:
			xSemaphoreTake(semphr_esp8266_cpltcall, portMAX_DELAY);
			pasos_conexion = paso_siguiente;
			break;
		default:
			break;

		}

	}

}

void task_server(void *arg)
{
	xSemaphoreTake(semphr_esp8266_conexion, portMAX_DELAY);
	comandos_t cmd_rx, last_cmd_rx = NONE;
	//char *data[PAGE_TX] = {0};
	esp_dato_t recibido;
	uint32_t muestra_recibida;
	//cmd_rx = NONE;
	esp8266_tx(DESCONEXION_PAGE, strlen((char*) DESCONEXION_PAGE));
	while(1)
	{
		xQueueReceive(queue_esp_enviar, &recibido, portMAX_DELAY);
		switch (recibido.tipo)
		{
		case MUESTRA:
			muestra_recibida = recibido.dato;
			break;
		case ALERTA_MEDICIONES_INESTABLES:
			break;
		case ALERTA_MEDICIONES_NULAS:
			break;
		}
		//verificar si se reciben alertas

		cmd_rx = esp8266_rx_comando(last_cmd_rx);
		//server_handle(NONE);
		if ((cmd_rx != NONE) && (cmd_rx != last_cmd_rx))
		{
			switch(cmd_rx)
			{
			case CONEXION:
				prender_led_bp();
				GPIO_habilitar_alimentacion_equipo();
				last_cmd_rx = cmd_rx;
				snprintf((char*)pag_tx, PAGE_TX, "%s%lu%s", CONEXION_ALT_PAGE, muestra_recibida, FINAL_CONEXION);
				esp8266_tx((char*)pag_tx, strlen((char*) pag_tx));
				//esp8266_tx(CONEXION_PAGE, strlen((char*) CONEXION_PAGE));
				break;
			case DESCONEXION:
				apagar_led_bp();
				GPIO_interrumpir_alimentacion_equipo();
				last_cmd_rx = cmd_rx;
				esp8266_tx(DESCONEXION_PAGE, strlen((char*) DESCONEXION_PAGE));
				break;
			case CONSUMO:
				last_cmd_rx = cmd_rx;
				snprintf((char*)pag_tx, PAGE_TX, "%s%lu%s", CONEXION_ALT_PAGE, muestra_recibida, FINAL_CONEXION);
				esp8266_tx((char*)pag_tx, strlen((char*) pag_tx));
				break;
			case CONSUMO_ALT:
				last_cmd_rx = cmd_rx;
				snprintf((char*)pag_tx, PAGE_TX, "%s%lu%s", CONEXION_PAGE, muestra_recibida, FINAL_CONEXION);
				esp8266_tx((char*)pag_tx, strlen((char*) pag_tx));
				//esp8266_tx(CONEXION_PAGE, strlen((char*) CONEXION_PAGE));
				break;
			default:
				break;
			/*case NONE:
				esp8266_tx(LED_OFF_PAGE, strlen((char*) LED_OFF_PAGE));
				break;*/
			}

			vTaskDelay(100);
			xSemaphoreTake(semphr_esp8266_conexion, portMAX_DELAY);
		}
	}
}
/*==================[end of file]============================================*/


