/**
  ******************************************************************************
  * @file           : comunicacion.c
  * @brief          : Procesamiento de comandos.
  *
  ******************************************************************************
  */

/*==================[inclusions]=============================================*/
#include <intercomunicacion.h>
#include "main.h"
#include "sensor_corriente.h"

#include "task.h"
#include "semphr.h"
#include <stdio.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
#define UINT16_DIGITOS_MAX 5

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
extern UART_HandleTypeDef huart2;

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void intercomunicacion_init(void)
{
	queue_esp_enviar = xQueueCreate(3, sizeof(esp_dato_t));

	if (queue_esp_enviar == NULL)
	{
		//Memoria insuficiente, no se creo la cola
	}

}

/*==================[end of file]============================================*/
