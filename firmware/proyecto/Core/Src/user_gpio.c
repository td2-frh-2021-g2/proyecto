/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/



/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/



/*==================[external functions definition]==========================*/

void GPIO_habilitar_alimentacion_equipo(void)
{
	HAL_GPIO_WritePin(SWITCH_ALIMENTACION_GPIO_Port, SWITCH_ALIMENTACION_Pin, GPIO_PIN_SET);
}

void GPIO_interrumpir_alimentacion_equipo(void)
{
	HAL_GPIO_WritePin(SWITCH_ALIMENTACION_GPIO_Port, SWITCH_ALIMENTACION_Pin, GPIO_PIN_RESET);
}

void prender_led_bp(void)
{
	HAL_GPIO_WritePin(LED_BP_GPIO_Port, LED_BP_Pin, GPIO_PIN_SET);
}
void apagar_led_bp(void)
{
	HAL_GPIO_WritePin(LED_BP_GPIO_Port, LED_BP_Pin, GPIO_PIN_RESET);
}
void toggle_led(void)
{
	HAL_GPIO_TogglePin(LED_BP_GPIO_Port, LED_BP_Pin);
}

void set_esp_en(void)
{
	HAL_GPIO_WritePin(ESP_EN_GPIO_Port, ESP_EN_Pin, GPIO_PIN_SET);
}

void reset_esp_en(void)
{
	HAL_GPIO_WritePin(ESP_EN_GPIO_Port, ESP_EN_Pin, GPIO_PIN_RESET);
}

void set_esp_rst(void)
{
	HAL_GPIO_WritePin(ESP_RST_GPIO_Port, ESP_RST_Pin, GPIO_PIN_SET);
}

void reset_esp_rst(void)
{
	HAL_GPIO_WritePin(ESP_RST_GPIO_Port, ESP_RST_Pin, GPIO_PIN_RESET);
}

/*==================[end of file]============================================*/
